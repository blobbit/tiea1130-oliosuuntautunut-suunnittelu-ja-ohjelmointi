function lisaaHissi() {
    var div = document.createElement('div');
    div.className = 'hissi';

    var posX = (currentHissiX += HISSI_WIDTH + HORIZONTAL_MARGIN);

    div.style.marginLeft = posX + 'px';
    div.style.marginTop = ((KERROKSET - 1) * (KERROS_HEIGHT + VERTICAL_MARGIN)) + 'px';
    document.getElementById('hissit').appendChild(div);

    const hissi = new Hissi(div);

    for (let i = 1; i <= KERROKSET; i++) {
        let nappi = document.createElement('button');
        nappi.appendChild(document.createTextNode(i));
        div.appendChild(nappi);

        const k = new KerroksenValintaNappi(i, hissi);
        nappi.onclick = function () { k.tilaaKerros() };
    }

    return hissi;
}

function lisaaKerros(hissiManager, index) {
    const ul = document.getElementById('kerrokset');

    const li = document.createElement('li');
    ul.appendChild(li);

    const divNumero = document.createElement('div');
    divNumero.className = 'kerros';
    divNumero.appendChild(document.createTextNode(index.toString()));
    li.appendChild(divNumero);

    const divNappulat = document.createElement('div');
    divNappulat.className = 'kerrosnappulat';
    li.appendChild(divNappulat);

    const huoltajaLabel = document.createElement('label');
    huoltajaLabel.appendChild(document.createTextNode('Huoltaja'));
    huoltajaLabel.htmlFor = 'huoltaja' + index;
    divNappulat.appendChild(huoltajaLabel);

    const huoltajaInput = document.createElement('input');
    huoltajaInput.type = 'checkbox';
    huoltajaInput.id = 'huoltaja' + index;
    divNappulat.appendChild(huoltajaInput);

    const tilausNappi = new TilausNappi(index, hissiManager);

    const buttonYlos = document.createElement('button');
    buttonYlos.appendChild(document.createTextNode('yl\u00F6s'));
    buttonYlos.onclick = function () { tilausNappi.tilaaHissi('ylos', huoltajaInput.checked); };
    divNappulat.appendChild(buttonYlos);


    const buttonAlas = document.createElement('button');
    buttonAlas.appendChild(document.createTextNode('alas'));
    buttonAlas.onclick = function () { tilausNappi.tilaaHissi('alas', huoltajaInput.checked); };
    divNappulat.appendChild(buttonAlas);
}

var currentHissiX = 300 - HISSI_WIDTH; // ekan hissin positio
window.onload = function () {
    var url = new URL(window.location.href);
	let param = parseInt(url.searchParams.get("kerrokset"));
	if (param > 1)
		KERROKSET = param;

    var huolossaInput = document.getElementById('huollossa');

    const hissiManager = new HissiManager();
    hissiManager.lisaaHissi(lisaaHissi()); // luodaan eka hissi

    huolossaInput.onchange = function () { hissiManager.huollossa = document.getElementById('huollossa').checked; };

    for (let i = KERROKSET; i > 0; i--)
        lisaaKerros(hissiManager, i);

    document.getElementById('lisaaHissi').onclick = function () {
        hissiManager.lisaaHissi(lisaaHissi());
    }

    setInterval(function () { hissiManager.liikutaHisseja(); }, 30);
}