const KERROS_HEIGHT = 100, VERTICAL_MARGIN = 4, HORIZONTAL_MARGIN = 10, HISSI_WIDTH = 50, ODOTUSAIKA = 10; // globaalit vakiot
var KERROKSET = 6;

class HissiManager {
    constructor() {
        this.hissit = [];
        this.huollossa = false;
    }

    lisaaHissi(hissi) {
        this.hissit.push(hissi);
        console.log('Hissi lis\u00E4tty!');
    }

    tilaaLahinHissi(kerros, suunta, isAdmin) {
        if (this.huollossa && !isAdmin) return;
        this.hissit.sort(function (a, b) { return a.kokonaisMatka(kerros) - b.kokonaisMatka(kerros); });
        return this.hissit[0];
    }

    liikutaHisseja() {
        for (let i of this.hissit)
            i.liiku();
    }
}

class Hissi {
    constructor(element) {
        this.nykyinenKerros = 1;
        this.tilaukset = [];
        this.element = element;
        this.odotusaika = 0;
    }

    liiku() {
        if (this.tilaukset.length == 0) return;

        if (this.odotusaika > 0) {
            this.odotusaika--;
            return;
        }

        const kerros = this.tilaukset[0];
        const haluttuMargin = (KERROKSET - kerros) * (KERROS_HEIGHT + VERTICAL_MARGIN);

        var currentMargin = parseInt(this.element.style.marginTop);

        if (kerros < this.nykyinenKerros) // alaspain
            currentMargin++;
        else if (kerros > this.nykyinenKerros) // ylospain
            currentMargin--;

        this.element.style.marginTop = currentMargin + 'px';

        if (haluttuMargin == currentMargin) {// ollaan saavuttu
            this.nykyinenKerros = this.tilaukset.shift();
            this.odotusaika = ODOTUSAIKA;
        }
    }

    tilaa(kerros) {
        //if (kerros == this.nykyinenKerros) return; // jos ollaan jo kerroksessa ei tehda mitaan
        console.log('Tilattu kerrokseen ' + kerros);
        this.tilaukset.push(kerros);
    }

    kokonaisMatka(kerros) {
        var total = Math.abs(kerros - this.nykyinenKerros);

        if (this.tilaukset.length > 0)
            total = Math.abs(this.tilaukset[0] - this.nykyinenKerros);
        else return total;

        if (this.tilaukset[0] == kerros) return Math.abs(kerros - this.nykyinenKerros);

        for (let i = 1; i < this.tilaukset.length; i++) {
            total += Math.abs(this.tilaukset[i - 1] - this.tilaukset[i]);
            if (this.tilaukset[i] == kerros) return total;
        }

        return total + Math.abs(kerros - this.tilaukset[this.tilaukset.length-1]);
    }
}

class TilausNappi {
    constructor(kerros, hissiManager) {
        this.kerros = kerros;
        this.hissiManager = hissiManager;
    }

    tilaaHissi(suunta, isAdmin) {
        console.log(this.kerros + ': ' + suunta + ', isAdmin: ' + isAdmin.toString());

        const hissi = this.hissiManager.tilaaLahinHissi(this.kerros, suunta, isAdmin);
        if (hissi != undefined) hissi.tilaa(this.kerros);
    }
}

class KerroksenValintaNappi {
    constructor(kerros, hissi) {
        this.kerros = kerros;
        this.hissi = hissi;
    }

    tilaaKerros() {
        this.hissi.tilaa(this.kerros);
    }
}
