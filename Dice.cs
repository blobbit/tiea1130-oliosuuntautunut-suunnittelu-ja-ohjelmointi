﻿using System;

class Dice
{
	static void Main(string[] args)
	{
		Die d = new Die(20);
		for (int i = 0; i < 20; i++)
			Console.WriteLine(d.Roll());

		Console.ReadKey();
	}
}

class Die
{
	private Random r;
	private int faces;

	public Die(int setFaces)
	{
		faces = setFaces;
		r = new Random();
	}

	public int Roll()
	{
		return r.Next(faces) + 1;
	}
}
